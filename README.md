# Arhitectura hardware pentru decodificarea Viterbi a codurilor convoluționale

adresă: https://gitlab.upt.ro/sabina.cenda/arhitectura-hardware-pentru-decodificarea-viterbi-a-codurilor-convolu-ionale

&emsp;&emsp;Acest proiect descrie proiectarea arhitecturii unui codificator convoluțional și a unui decodificator Viterbi, urmată de verificarea și implementarea celor două componente. Codul convoluțional are rata 1/2, lungimea constrângerii egală cu 5 și polinoamele generatoare {23, 35} în notație octală.

## Descrierea fișierelor sursă

&emsp;&emsp;Fișierele proiectului sunt împărțite în două categorii, fișiere C și fișiere HDL (Hardware Description Language).

#### Fișiere C

&emsp;&emsp;Fișierele C au rolul de a asista procesului de verificare a sistemului, fiind utilizată interfața SystemVerilog DPI-C în acest scop. Directorul este împărțit în trei subdirectoare, fiecare dintre acestea conținând câte un fișier C, un fișier header, un fișier text cu date de intrare și un fișier text cu date de ieșire. Cele trei subdirectoare corespund codificatorului convoluțional, decodificatorului Viterbi și canalului de comunicare simulat (binary symmetric channel).

&emsp;&emsp;În cadrul acestui director se regăsește un fișier C, două fișiere header, și două fișiere text. Biblioteca svdpi.h conține declarații necesare conversiei de date dintre SystemVerilog și C. Aceasta a fost preluată din fișierele puse la dispoziție de Vivado Design Suite în scopul utilizării interfeței SV DPI-C. Fișierul wrappers.c are rolul de a declara funcții care împachetează codifcatorul, decodificatorul și canalul pentru a putea fi apelate considerând conversia de date necesară pentru utilizarea interfeței SV DPI-C. Atât acest fișier C, cât si cele menționate anterior au conținutul funcției main comentat. Dacă acest cod este decomentat toate fișierele se pot rula separat, inclusiv wrappers.c care simulează sistemul codificator - canal - decodificator.

#### Fișiere HDL

&emsp;&emsp;Fișierele HDL se împart în trei directoare: codificator convoluțional, decodificator Viterbi și Top.

&emsp;&emsp;Codificatorul conține cinci tipuri de fișiere împărțite în subdirectoare separate: fișiere Matlab, un pachet, 5 surse de design, un fișier de constrângeri și un testbench. Fișierele Matlab constă într-o sursă și două fișiere text pentru codificarea unor vectori de test folosind funcțiile predefinite poly2trellis și convec. Pachetul conține parametrii codului convoluțional și alte constante utile în implementare, fișierul de costrângeri este utilizat la implementare pentru a seta perioada tactului, iar testbench-ul este folosit pentru verificarea pe parcurs a modulelor codificatorului. Fișierele de design constă în codificatorul propriu-zis, înterfețe pentru împachetarea și despachetarea pachetelor de date pentru a fi generate codificatorului, respectiv scoase ca și ieșire, o interfață care instanțiază cele trei module și este compatibilă protocolului AXI-Stream și un modul adițional cu rol de a întârzia semnale pentru a sincroniza procesul de codificare.

&emsp;&emsp;Decodificatorul conține patru tipuri de fișiere împărțite în subdirectoare separate: 3 pachete, 7 surse de design, un fișier de constrângeri și un testbench. Cele 3 pachete, similar cu pachetul de la codificator, au rolul de a defini parametrii codului, constante, funcții și tipuri de date, fișierul de constrângeri este similar cu cel al codificatorului, urmând să fie utilizat în cadrul implementării, iar testbench-ul este folosit pentru verificarea pe parcurs a modulelor decodificatorului, folosind ca și date de referință aceleași fișiere generate în Matlab anterior. Fișierele de design constă în cele 3 componente principale ale decodificatorului Viterbi, BMU(Branch Metric Unit), ACS(Add Compare Select unit) și SMU(Survivor Management Unit), două module auxiliare, unul de memorie RAM și unul pentru calcularea stării de pornire pentru a doua jumătate a algoritmului, modulul decodificatorului Viterbi în cadrul căruia sunt instanțiate modulele menționate anterior și un modul care împachetează decodificatorul într-o interfață compatibilă protocolului AXI-Stream.

&emsp;&emsp;Directorul Top conține fișierul testbench responsabil cu verificarea sistemului împreună cu un director adițional care conține două fișiere de tip script pentru compilarea și simularea în ModelSim.

## 

## Pentru a compila fișierele C separat se urmează următorii pași:

Se deschide un terminal și se scrie următoarea comandă:

```
cd cale/către/directorul/proiectului/C
```

#### Compilarea decodificatorului

Se decomentează codul din funcția main a fișierului decoder.c și apoi se aplică următoarele comenzi în terminal:

```
cd decoder
gcc -o decoder decoder.c
./decoder
```

Similar se pot compila separat codificatorul și canalul.

#### Compilarea sistemului codificator - canal - decodificator

Se decomentează codul din funcția main a fișierului wrappers.c și apoi se aplică următoarele comenzi în terminal:

```
gcc -o wrappers wrappers.c encoder/encoder.c decoder/decoder.c channel/bsc.c
./wrappers
```

## 

## Pentru a simula sistemul se urmează următorii pași:
    
#### Deschiderea directorului în ModelSim

Se deschide ModelSim și se scrie în fereastra „transcript” următoarea comandă:

```
cd cale/către/directorul/proiectului/VHDL/Top/scripts
```

#### Compilarea modulelor VHDL și a testbench-ului SystemVerilog

În fereastra „transcript” se scrie următoarea comandă:

```
do compile.do
```

#### Simularea sistemului

În fereastra „transcript” se scrie următoarea comandă:

```
do simulate.do
```

#### Adăugarea semnalelor pe wave

În fișierul simulate.do se inserează înainte de run -all următoarea comandă:

```
add wave -radix hex -position insertpoint sim:/dpi_c_tb/nume_instanță/*
```

Spre exemplu, pentru semnalele modulului Viterbi nume_instanță se înlocuiește cu decoder/dec

## 

## Pentru a implementa codificatorul se urmează următorii pași:

* Se lansează Vivado Design Suite
* De pe pagina de pornire se crează un nou proiect folosind butonul „Create Project” din fereastra „Quick Start” 
* În fereastra „New project” se apasă butonul „next”, se denumește proiectul și se alege locația acestuia, apoi se apasă din nou „next”
* Se selectează „RTL Project” și apoi „next”
* Se apasă butonul „Add Files” și în fereastra deschisă se adaugă următoarele surse și apoi se apasă „next”:
    * VHDL/Convolutional Encoder/pkg/pkg_parameters_cc
    * VHDL/Convolutional Encoder/src/delay_buffer.vhd, encoder.vhd, slave_axi_s_interface.vhd, master_axi_s_interface.vhd, axis_conv_encoder
    * VHDL/Convolutional Encoder/tb/encoder_tb.vhd - opțional (dacă se dorește simularea unui exemplu specific), HDL Source For Simulation only
* Se apasă butonul „Add Files” și în fereastra deschisă se adaugă fișierul  VHDL/ConvolutionalEncoder/constraints/systemclk.xdc și apoi se apasă „next”
* Se scrie în bara de căutare „xcku035-ffva1156-1LV-I”, se selectează partea apărută și apoi se apasă „next”
* Se apasă „Finish”
* Se selectează toate sursele, click dreapta, „Set Library...”, Library name: work, „OK”
* Pentru a începe sinteza, se apasă butonul „Run Synthesis” din Flow Navigator/SYNTHESIS
* După finalizarea sintezei, din pop-up-ul apărut se selectează „Run Implementation” și apoi „OK”
* După finalizarea implementării,  din pop-up-ul apărut se selectează „Open Implemented Design”
* Rezultatele generate în urma implementării se pot vizualiza în partea de jos a aplicației (Reports, Power, Timing etc)
* Pentru a vizualiza schematica se apasă „Open Elaborated Design” din Flow Navigator/RTL ANALYSIS

##

## Pentru a implementa decodificatorul se urmează următorii pași:

* Se repetă pașii de la implementarea codificatorului cu sursele următoare selectate în pașii 5 și 6:
    * VHDL/Viterbi Decoder/pkg/pkg_func.vhd, pkg_parameters.vhd, pkg_data_types.vhd
    * VHDL/Viterbi Decoder/src/BMU.vhd, ACS.vhd, RAM_mem.vhd, SMU.vhd, start_node.vhd, Viterbi.vhd, axis_viterbi_decoder.vhd
    * VHDL/Viterbi Decoder/tb/decoder_tb.vhd - opțional (dacă se dorește simularea unui exemplu specific), HDL Source For Simulation only
    * VHDL/Viterbi Decoder/constraints/systemclk.xdc
