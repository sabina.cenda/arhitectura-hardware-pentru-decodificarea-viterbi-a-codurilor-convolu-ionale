library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


package pkg_parameters_cc is

	--
	-- Set code rate - number of parity bits
	--
	constant rate : natural := 2;
	
	--
	-- Set constraint length 
	--
	constant k_length : natural := 5;
	
	--
	-- Set parity polynomials in decimal notation
    --
    type t_parity is array (0 to rate - 1) of std_logic_vector(0 to k_length - 1);
	constant PARITY_POLYNOMIALS   : t_parity := ("10011", "11101"); -- (19, 29)dec; (23, 35)oct
	
	--
	-- Set input stream size and decoded output size
    --
    constant input_size : natural := 128;
    constant output_size : natural := input_size * rate;
    
	--
	-- Set encoder sizes
    --
    constant input_size_enc : natural := k_length;
    constant output_size_enc : natural := input_size_enc * rate;

end package pkg_parameters_cc;