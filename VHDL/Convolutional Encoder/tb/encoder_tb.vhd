library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

library work;
use work.pkg_parameters_cc.all;

entity conv_encoder_tb is
end;

architecture bench of conv_encoder_tb is

  component axis_conv_encoder
       Port ( clk           : in std_logic;
              ARESET_N      : in std_logic;
              S_TVALID      : in std_logic;
              S_TREADY      : out std_logic;
              S_TDATA       : in std_logic_vector(input_size - 1 downto 0);
              S_TSTRB       : in std_logic_vector(input_size_enc - 1 downto 0);
              S_TKEEP       : in std_logic_vector(input_size_enc - 1 downto 0);
              S_TLAST       : in std_logic;
              M_TVALID      : out std_logic;
              M_TREADY      : in  std_logic;
              M_TDATA       : out std_logic_vector(output_size - 1 downto 0);
              M_TSTRB       : out std_logic_vector(output_size_enc - 1 downto 0);
              M_TKEEP       : out std_logic_vector(output_size_enc - 1 downto 0);
              M_TLAST       : out std_logic
          );
  end component;

  signal clk: std_logic;
  signal ARESET_N: std_logic;
  signal S_TVALID: std_logic;
  signal S_TREADY: std_logic;
  signal S_TDATA: std_logic_vector(input_size - 1 downto 0);
  signal S_TSTRB: std_logic_vector(input_size_enc - 1 downto 0);
  signal S_TKEEP: std_logic_vector(input_size_enc - 1 downto 0);
  signal S_TLAST: std_logic;
  signal M_TVALID: std_logic;
  signal M_TREADY: std_logic;
  signal M_TDATA: std_logic_vector(output_size - 1 downto 0);
  signal M_TSTRB: std_logic_vector(output_size_enc - 1 downto 0);
  signal M_TKEEP: std_logic_vector(output_size_enc - 1 downto 0);
  signal M_TLAST: std_logic ;
  
  constant clock_period: time := 10 ns;
  signal stop_the_clock: boolean;

begin

  uut: axis_conv_encoder port map ( clk      => clk,
                                    ARESET_N => ARESET_N,
                                    S_TVALID => S_TVALID,
                                    S_TREADY => S_TREADY,
                                    S_TDATA  => S_TDATA,
                                    S_TSTRB  => S_TSTRB,
                                    S_TKEEP  => S_TKEEP,
                                    S_TLAST  => S_TLAST,
                                    M_TVALID => M_TVALID,
                                    M_TREADY => M_TREADY,
                                    M_TDATA  => M_TDATA,
                                    M_TSTRB  => M_TSTRB,
                                    M_TKEEP  => M_TKEEP,
                                    M_TLAST  => M_TLAST );

  stimulus: process
  begin
  
    -- Put initialisation code here
    S_TSTRB <= (others => '1');
    S_TKEEP <= (others => '1');
    S_TLAST <= '1';
    S_TVALID <= '0';
    ARESET_N <= '0';
    wait for 10 ns;
    ARESET_N <= '1';
    M_TREADY <= '1';
    wait for 10 ns;
    
    S_TVALID <= '1';
    S_TDATA <= "01010011001000001100110110110111100101010010111110000111000100011001010010111101101111001001001011010101001110100011101110000001"; 
    --S_TDATA <= (others => '0');
    --S_TDATA <= "1101010101010111";
    wait for 10 ns;
    --S_TDATA <= "00000000";
    S_TVALID <= '0';
    
--    wait until S_TREADY = '1';
--    S_TVALID <= '1';
--    S_TDATA <= "01101110001010101010111111000101100110011101110001000100101100100110000110110110100110000100001100010010100000011001000110111100"; 
--    wait for 10 ns;
--    S_TVALID <= '0';

    wait;
  end process;

  clocking: process
  begin
    while not stop_the_clock loop
      clk <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;

end;