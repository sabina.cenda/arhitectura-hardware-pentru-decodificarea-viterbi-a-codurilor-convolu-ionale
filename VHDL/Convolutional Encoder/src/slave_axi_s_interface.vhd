library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library work;
use work.pkg_parameters_cc.all;


entity slave_axi_s_interface is
    port(ACLK       : in std_logic;
        ARESET_N    : in std_logic;
        TVALID      : in std_logic;
        TREADY      : out std_logic;
        TDATA       : in std_logic_vector(input_size - 1 downto 0);
        TLAST       : in std_logic;
        
        samples     : out std_logic_vector(input_size_enc - 1 downto 0);
        valid_sample: out std_logic
        );
end entity;

architecture slave of slave_axi_s_interface is
    
    signal data_reg, data_nxt			        : std_logic_vector(input_size - 1 downto 0);
    signal sample_reg, sample_nxt            	: std_logic_vector(input_size_enc - 1 downto 0);
    signal valid_sample_reg, valid_sample_nxt   : std_logic;
    signal idx_reg, idx_nxt			            : unsigned(k_length - 1 downto 0);
    signal ready_reg, ready_nxt 		        : std_logic;
    
begin 

    sample_delay: process(ACLK) 
    begin 
        if(ACLK' event and ACLK = '1') then 
            if(ARESET_N = '0') then 
                valid_sample_reg <= '0';
                sample_reg <= (others => '0');
                idx_reg <= (others => '0');
                ready_reg <= '1';
                data_reg <= (others => '0');
            else
                valid_sample_reg <= valid_sample_nxt;
                sample_reg <= sample_nxt;
                idx_reg <= idx_nxt;
                ready_reg <= ready_nxt;
                data_reg <= data_nxt;
            end if;
        end if;
        

        
    end process;
    
    comb_proc: process(TVALID, TDATA, TLAST, valid_sample_reg, sample_reg, idx_reg, ready_reg, data_reg)
  
    begin
    
    	idx_nxt <= idx_reg;
        data_nxt <= data_reg;
        valid_sample_nxt <= '0';
        ready_nxt <= ready_reg;
        sample_nxt <= sample_reg;
	

        if (ready_reg = '1' and TVALID = '1') then
        	data_nxt <= TDATA;
            ready_nxt <= '0';
        end if;
        
        if (ready_reg = '0' and idx_reg < input_size/input_size_enc + 1) then
            sample_nxt <= data_reg(input_size - 1 downto input_size - input_size_enc);
            data_nxt(input_size - 1 downto input_size_enc) <= data_reg(input_size - input_size_enc - 1 downto 0);
            data_nxt(input_size_enc - 1 downto 0) <= (others => '0');
            idx_nxt <= idx_reg + 1;
            valid_sample_nxt <= '1';
        else 
            valid_sample_nxt <= '0';
        end if;
        
        if (TLAST = '1') then
            idx_nxt <= (others => '0');
            ready_nxt <= '1';
            data_nxt <= (others => '0');
        end if;
    
    end process;

    samples <= sample_reg;
    valid_sample <= valid_sample_reg;
    TREADY <= ready_reg;
    
end architecture;
