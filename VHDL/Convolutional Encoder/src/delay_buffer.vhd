library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity delay_buffer is
    generic(DELAY   : natural := 1;
            SIZE    : natural := 8);
    port(clk, rst   : in std_logic;
        data_in     : in std_logic_vector(SIZE - 1 downto 0);
        data_out    : out std_logic_vector(SIZE - 1 downto 0));
end entity;

architecture behave of delay_buffer is 
    type vector_array is array(DELAY - 1 downto 0) of std_logic_vector(SIZE -1 downto 0);
    
    signal delay_elem : vector_array;
    
begin 

    delays: process(clk)
    begin 
        if(clk'event and clk = '1') then 
            if(rst = '0') then 
                for idx in 0 to DELAY - 1 loop
                    delay_elem(idx) <= (others => '0');
                end loop;
            else
                delay_elem(0) <= data_in;
                for idx in 1 to DELAY - 1 loop
                    delay_elem(idx) <= delay_elem(idx - 1);
                end loop;
            end if;
        end if;
    end process;
  
    data_out <= delay_elem(DELAY - 1);
    
end architecture;
