library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

library work;
use work.pkg_parameters_cc.all;


entity conv_encoder is
    Port ( in_source    : in std_logic_vector (input_size_enc - 1 downto 0);
           in_valid     : in std_logic;
           out_coded    : out std_logic_vector(output_size_enc - 1 downto 0);
           out_valid    : out std_logic;
           clk : in STD_LOGIC;
           rst : in STD_LOGIC);
end conv_encoder;

architecture Behavioral of conv_encoder is

    component delay_buffer
        generic(DELAY   : natural := 1;
                SIZE    : natural := input_size_enc);
        port(clk, rst   : in std_logic;
            data_in     : in std_logic_vector(SIZE - 1 downto 0);
            data_out    : out std_logic_vector(SIZE - 1 downto 0));
    end component;

    signal in_source_reg:	std_logic_vector(rate * input_size_enc - 1 downto 0);
    signal in_source_nxt:	std_logic_vector(rate * input_size_enc - 1 downto 0);
    signal out_coded_reg:	std_logic_vector(output_size_enc - 1 downto 0);
    signal out_coded_nxt:	std_logic_vector(output_size_enc - 1 downto 0);
    
    signal inv:             std_logic_vector(0 downto 0);
    signal outv:            std_logic_vector(0 downto 0);

begin

db: delay_buffer generic map (    DELAY    => 2,
                                  SIZE     =>  1)
                       port map ( clk      => clk,
                                  rst      => rst,
                                  data_in  => inv,
                                  data_out => outv );
                               
seq_process: PROCESS(clk)
	
BEGIN

    if(clk'event and clk='1') then 
			if(rst = '0') then 
		      out_coded_reg <= (others => '0');
		      in_source_reg <= (others => '0');
		    else
		      out_coded_reg <= out_coded_nxt;
		      in_source_reg <= in_source_nxt;
		    end if;
    end if;
END PROCESS;

comb_process: PROCESS(in_source, in_valid, in_source_reg, out_coded_reg)
BEGIN

    in_source_nxt <= in_source_reg;
    
	IF in_valid='1' THEN

	   in_source_nxt(input_size_enc - 1 downto 0) <= in_source_reg(rate * input_size_enc - 1 downto input_size_enc);
	   for i in 0 to input_size_enc - 1 loop
	       in_source_nxt(input_size_enc + i) <= in_source(input_size_enc - 1 - i);
	   end loop;
	ELSE 
	   in_source_nxt <= (others => '0');
	END IF;

END PROCESS;

    gen: for i in 0 to input_size_enc - 1 generate
        signal x_i: std_logic_vector(k_length - 1 downto 0);
        signal coded_i: std_logic_vector(rate - 1 downto 0);
    begin
	       x_i <= in_source_reg((input_size_enc - 1 + i) downto (input_size_enc - k_length + i));
	       
	       gen2: for j in 0 to rate - 1 generate
	           signal res_j: std_logic_vector(k_length - 1 downto 0);
	       begin
	       
	           res_j(k_length - 1 downto 0) <= PARITY_POLYNOMIALS(j) and x_i;
	           coded_i(j) <= res_j(0) xor res_j(1) xor res_j(2) xor res_j(3) xor res_j(4);
	       
	       end generate gen2;
	       
	       out_coded_nxt(rate * i + 1 downto rate * i) <= coded_i;
        end generate gen;

    inv(0) <= in_valid;
    out_valid <= outv(0);
    out_coded <= out_coded_reg;

end Behavioral;

