library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library work;
use work.pkg_parameters_cc.all;


entity axis_conv_encoder is
     Port ( clk           : in std_logic;
            ARESET_N      : in std_logic;
        
            S_TVALID      : in std_logic;
            S_TREADY      : out std_logic;
            S_TDATA       : in std_logic_vector(input_size - 1 downto 0);
            S_TSTRB       : in std_logic_vector(input_size_enc - 1 downto 0);
            S_TKEEP       : in std_logic_vector(input_size_enc - 1 downto 0);
            S_TLAST       : in std_logic;
            
            M_TVALID      : out std_logic;
            M_TREADY      : in  std_logic;
            M_TDATA       : out std_logic_vector(output_size - 1 downto 0);
            M_TSTRB       : out std_logic_vector(output_size_enc - 1 downto 0);
            M_TKEEP       : out std_logic_vector(output_size_enc - 1 downto 0);
            M_TLAST       : out std_logic
        );
end axis_conv_encoder;

architecture Behavioral of axis_conv_encoder is

    component conv_encoder
      Port ( in_source :    in std_logic_vector (input_size_enc - 1 downto 0);
             in_valid :     in std_logic;
             out_coded :    out std_logic_vector(output_size_enc - 1 downto 0);
             out_valid :    out std_logic;
             clk :          in STD_LOGIC;
             rst :          in STD_LOGIC);
  end component;
  
  component slave_axi_s_interface
    port(ACLK       : in std_logic;
        ARESET_N    : in std_logic;
        TVALID      : in std_logic;
        TREADY      : out std_logic;
        TDATA       : in std_logic_vector(input_size - 1 downto 0);
        TLAST       : in std_logic;
        samples     : out std_logic_vector(input_size_enc - 1 downto 0);
        valid_sample: out std_logic
        );
end component;

component master_axi_s_interface is
    port(ACLK       : in std_logic;
        ARESET_N    : in std_logic;
        TVALID      : out std_logic;
        TREADY      : in std_logic;
        TDATA       : out std_logic_vector(output_size - 1 downto 0);
        TLAST       : out std_logic;
        samples     : in std_logic_vector(output_size_enc - 1 downto 0);
        valid_sample: in std_logic
        );
end component;

  signal in_source: std_logic_vector (input_size_enc - 1 downto 0);
  signal in_valid:  std_logic;
  signal out_coded: std_logic_vector(output_size_enc - 1 downto 0);
  signal out_valid: std_logic;
  signal TREADY:    std_logic; 
  signal TLAST :    std_logic; 
  signal TVALID :    std_logic; 
  signal rst :      std_logic;

begin

  uut: conv_encoder port map ( in_source => in_source,
                               in_valid  => in_valid,
                               out_coded => out_coded,
                               out_valid => out_valid,
                               clk       => clk,
                               rst       => rst );
                               
  slv: slave_axi_s_interface port map(  ACLK            => clk,
                                        ARESET_N        => ARESET_N,
                                        TVALID          => S_TVALID,
                                        TREADY          => S_TREADY,
                                        TDATA           => S_TDATA,
                                        TLAST           => TLAST,                                     
                                        samples         => in_source,
                                        valid_sample    => in_valid );
                                        
  mas: master_axi_s_interface port map(  ACLK           => clk,
                                        ARESET_N        => ARESET_N,
                                        TVALID          => TVALID,
                                        TREADY          => M_TREADY,
                                        TDATA           => M_TDATA,
                                        TLAST           => TLAST,                                       
                                        samples         => out_coded,
                                        valid_sample    => out_valid );

  comb_proc: process (ARESET_N, TVALID)
    begin

      if(ARESET_N = '0' or TVALID = '1') then
        rst <= '0';
      else
      
        rst <= '1';
      
      end if;

    end process;

  M_TVALID <= TVALID;
  M_TLAST <= TLAST;
  M_TSTRB <= S_TSTRB & S_TSTRB;
  M_TKEEP <= S_TKEEP & S_TKEEP;

end Behavioral;

