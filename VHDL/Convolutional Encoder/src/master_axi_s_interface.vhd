library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library work;
use work.pkg_parameters_cc.all;


entity master_axi_s_interface is
    port(ACLK       : in std_logic;
        ARESET_N    : in std_logic;
        TVALID      : out std_logic;
        TREADY      : in std_logic;
        TDATA       : out std_logic_vector(output_size - 1 downto 0);
        TLAST       : out std_logic;
        
        samples     : in std_logic_vector(output_size_enc - 1 downto 0);
        valid_sample: in std_logic
        );
end entity;

architecture master of master_axi_s_interface is 

    signal idx_reg, idx_nxt                 	: unsigned(k_length - 1 downto 0);
    signal data_reg, data_nxt                   : std_logic_vector(output_size - 1 downto 0);
    signal data_in_reg, data_in_nxt             : std_logic_vector(output_size - 1 downto 0);
    signal valid_out, valid_out_nxt             : std_logic;

begin 
    
    sample_delay: process(ACLK)  
    begin 
    
        if(ACLK'event and ACLK = '1') then 
            
            if(ARESET_N = '0') then 
                data_reg <= (others => '0');
                idx_reg <= (others => '0');
                data_in_reg <= (others => '0');
                valid_out <= '0';
            else
                data_reg  <= data_nxt;
                idx_reg <= idx_nxt;
                data_in_reg <= data_in_nxt;
                valid_out <= valid_out_nxt;
            end if;
            
        end if; 
    
    end process;
    
    comb_proc: process (TREADY, samples, valid_sample, data_reg, idx_reg, data_in_reg, valid_out)
    begin

           idx_nxt <= idx_reg;
           valid_out_nxt <= valid_out;

           data_nxt <= data_reg;
           data_in_nxt <= data_in_reg;
           
	       if idx_reg < input_size/input_size_enc and valid_sample = '1' then
	           data_nxt(output_size - output_size_enc - 1 downto 0) <= data_reg (output_size - 1 downto output_size_enc);
	           data_nxt(output_size - 1 downto output_size - output_size_enc) <= samples;
	           idx_nxt <= idx_reg + 1;
	       end if;
	       
	       if(idx_reg = input_size/input_size_enc and valid_sample = '1') then
	           data_nxt(output_size - output_size_enc - 1 + (input_size_enc - (input_size mod input_size_enc)) downto 0) <= data_reg (output_size - 1 downto output_size_enc - (input_size_enc - (input_size mod input_size_enc)));
	           data_nxt(output_size - 1 downto output_size - output_size_enc + (input_size_enc - (input_size mod input_size_enc))) <= samples(output_size_enc - (input_size_enc - (input_size mod input_size_enc)) - 1 downto 0);
	           idx_nxt <= idx_reg + 1;
	       end if;
           
            if (idx_reg = input_size/input_size_enc + 1 and TREADY = '1') then 
                for i in 0 to output_size - 1 loop
                    data_in_nxt(i) <= data_reg(output_size - 1 - i);
                end loop;
                valid_out_nxt <= '1';
                idx_nxt <= (others => '0');
            else
                valid_out_nxt <= '0';
            end if;

    end process;
    
    TDATA <= data_in_reg;
    TVALID <= valid_out;
    TLAST <= valid_out;
    
end architecture;
