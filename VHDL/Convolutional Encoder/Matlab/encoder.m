
fileID_in = fopen("in.txt", "r");
fileID_out = fopen("out.txt", "w");

for i = 1:50
   data_in = fscanf(fileID_in, "%c", 128);
   fscanf(fileID_in, "%c", 2);
   data_in = data_in == '1';

   for k = 1:128
       fprintf('%d', data_in(k));
   end
   fprintf('\n');
   
    trellis = poly2trellis(5, [23 35]);
    data_out = convenc(data_in, trellis);

    for k=1:256
        fprintf(fileID_out,"%d", data_out(k));
    end

    fprintf(fileID_out,"\n");

end

fclose(fileID_out);
fclose(fileID_in);