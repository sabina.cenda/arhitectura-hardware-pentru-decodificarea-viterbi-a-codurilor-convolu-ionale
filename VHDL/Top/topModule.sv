module dpi_c_tb;

 parameter int input_data_encoder_size = 128;
 parameter int output_data_encoder_size = 256;
 parameter int k_length = 5;
 parameter int rate = 2;
 parameter int input_data_decoder_size = 256;
 parameter int output_data_decoder_size = 128;
 parameter int no_of_samples = 100;
 int eq[2];
 real channelProbability;
 
 // Declare DPI-C import function prototype
  import "DPI-C" function void readInputWrapper(
    output logic [0:0] vsource[input_data_encoder_size - 1:0],
    input int source_size
);

  import "DPI-C" function void convEncoderWrapper(
    input logic [0:0] vsource[input_data_encoder_size - 1:0],
    output logic [0:0] vcoded[output_data_encoder_size - 1:0],
    input int source_size,
    input int coded_size,
    input int k_length,
    input int rate,
    input int eq[2]
);

import "DPI-C" function void bscWrapper(
    input logic [0:0] bitsIn[output_data_encoder_size - 1:0],
    output logic [0:0] bits_out[output_data_encoder_size - 1:0],
    input int bits_size,
    input real probability
);

import "DPI-C" function void viterbiDecoderWrapper(
    input logic [0:0] encoded[input_data_decoder_size - 1:0],
    output logic [0:0] decoded[output_data_decoder_size - 1:0],
    input int encoded_size,
    input int decoded_size,
    input int rate,
    input int k_length,
    input int eq[2]
);

 //DPI-C signals
 logic [0:0] readData[input_data_encoder_size - 1:0];
 logic [input_data_encoder_size - 1:0] inputData;
 logic [0:0] encodedData[output_data_encoder_size - 1:0];
 logic [output_data_encoder_size - 1:0] out_encoder_c;
 logic [0:0] channelData[output_data_encoder_size - 1:0];
 logic [output_data_encoder_size - 1:0] out_channel;
 logic [0:0] decodedData[output_data_decoder_size - 1:0];
 logic [output_data_decoder_size - 1:0] out_decoder_c;

 //BER computation
 real sum;
 real BER;

 logic clk;
 logic ARESET_N;
  
 //encoder signals
 logic E_S_TVALID;
 logic E_S_TREADY;
 logic [input_data_encoder_size - 1:0] E_S_TDATA;
 logic [k_length - 1:0] E_S_TSTRB;
 logic [k_length - 1:0] E_S_TKEEP;
 logic E_S_TLAST;
 
 logic E_M_TVALID;
 logic E_M_TREADY;
 logic [output_data_encoder_size - 1:0] E_M_TDATA;
 logic [k_length * rate - 1:0] E_M_TSTRB;
 logic [k_length * rate - 1:0] E_M_TKEEP;
 logic E_M_TLAST;
 
 //decoder signals
 
 logic D_M_TVALID;
 logic D_M_TREADY;
 logic [input_data_decoder_size - 1:0] D_M_TDATA;
 logic D_M_TSTRB;
 logic D_M_TKEEP;
 logic D_M_TLAST;
 
 logic D_S_TVALID;
 logic D_S_TREADY;
 logic [output_data_decoder_size - 1:0] D_S_TDATA;
 logic D_S_TSTRB;
 logic D_S_TKEEP;
 logic D_S_TLAST;
 
 //Encoder instance
  axis_conv_encoder encoder (
    .clk(clk),
    .ARESET_N(ARESET_N),

    .S_TVALID(E_S_TVALID),
    .S_TREADY(E_S_TREADY),
    .S_TDATA(E_S_TDATA),
    .S_TSTRB(E_S_TSTRB),
    .S_TKEEP(E_S_TKEEP),
    .S_TLAST(E_S_TLAST),

    .M_TVALID(E_M_TVALID),
    .M_TREADY(E_M_TREADY),
    .M_TDATA(E_M_TDATA),
    .M_TSTRB(E_M_TSTRB),
    .M_TKEEP(E_M_TKEEP),
    .M_TLAST(E_M_TLAST)
  );

 //Decoder instance
  axis_viterbi_decoder decoder (
    .clk(clk),
    .ARESET_N(ARESET_N),

    .M_TVALID(D_M_TVALID),
    .M_TREADY(D_M_TREADY),
    .M_TDATA(D_M_TDATA),
    .M_TSTRB(D_M_TSTRB),
    .M_TKEEP(D_M_TKEEP),
    .M_TLAST(D_M_TLAST),
    
    .S_TVALID(D_S_TVALID),
    .S_TREADY(D_S_TREADY),
    .S_TDATA(D_S_TDATA),
    .S_TSTRB(D_S_TSTRB),
    .S_TKEEP(D_S_TKEEP),
    .S_TLAST(D_S_TLAST)
  );

  // Clock 
  initial begin
      clk = 0;
	  forever #5 clk <= ~clk; 
	end

  initial begin

  // Initialize input signals
  eq[0] = 19;
  eq[1] = 29;
  out_encoder_c = 0;
  out_channel = 0;
  out_decoder_c = 0;
  channelProbability = 0.05; //[0.01:0.1]
  sum = 0;
  BER = 0;
  ARESET_N <= 0;
  #10 ARESET_N <= 1;

  // Encoder signals
  E_S_TVALID = 0;
  E_M_TREADY = 0;
  E_S_TDATA = 0;
  E_S_TSTRB = 0;
  E_S_TKEEP = 0;
  E_S_TLAST = 0;
  
  // Decoder signals
  D_M_TVALID = 0;
  D_S_TREADY = 0;
  D_M_TDATA = 0;
  D_M_TSTRB = 0;
  D_M_TKEEP = 0;
  D_M_TLAST = 0;
  
for (integer i = 1; i <= no_of_samples; i = i + 1) begin

  $display("\nVector %0d - time %0t ps\n", i, $time);

  ARESET_N <= 0;
  #10 ARESET_N <= 1;

  readInputWrapper(readData, input_data_encoder_size);
  for (int i = 0; i < input_data_encoder_size; i++) begin
    inputData[i] = readData[i];
  end

  E_S_TVALID = 1;
  E_M_TREADY = 1;
  E_S_TDATA = inputData;
  E_S_TSTRB = 0;
  E_S_TKEEP = 0;
  E_S_TLAST = 1;

  convEncoderWrapper(readData, encodedData, input_data_encoder_size, output_data_encoder_size, k_length, rate, eq);
  for (int i = 0; i < output_data_encoder_size; i++) begin
    out_encoder_c[i] = encodedData[i];
  end

  @(posedge E_M_TVALID);
  assert (E_M_TDATA === out_encoder_c)
  $display("Assertion passed: encoder results are the same");
  else $error("Assertion failed: encoder results are different", E_M_TDATA);

  E_S_TVALID = 0;
  E_M_TREADY = 0;
  E_S_TSTRB = 0;
  E_S_TKEEP = 0;
  E_S_TLAST = 0;

  bscWrapper(encodedData, channelData, output_data_encoder_size, channelProbability);
  for (int i = 0; i < output_data_encoder_size; i++) begin
    out_channel[i] = channelData[i];
  end

  ARESET_N <= 0;
	#10 ARESET_N <= 1;
  
  D_M_TVALID = 1;
  D_S_TREADY = 1;
  D_M_TDATA = out_channel;
  D_M_TSTRB = 0;
  D_M_TKEEP = 0;
  D_M_TLAST = 1;

  #10

  D_M_TVALID = 0;
  D_M_TLAST = 0;

  viterbiDecoderWrapper(channelData, decodedData, input_data_decoder_size, output_data_decoder_size, rate, k_length, eq);
  for (int i = 0; i < output_data_decoder_size; i++) begin
    out_decoder_c[i] = decodedData[output_data_decoder_size - i - 1];
  end

  @(posedge D_S_TVALID);
  assert (D_S_TDATA === out_decoder_c)
  $display("Assertion passed: decoder results are the same");
  else $error("Assertion failed: decoder results are different");

  assert (D_S_TDATA === inputData)
  $display("Assertion passed: decoder results are the same as initial bitstream");
  else begin
    $display("Assertion failed: decoder results are different from initial bitstream");
    sum = sum + $countones(D_S_TDATA ^ inputData);
    $display("\nsum: %d\n\n", sum);
  end

  D_M_TVALID = 0;
  D_S_TREADY = 0;
  D_M_TDATA = out_channel;
  D_M_TSTRB = 0;
  D_M_TKEEP = 0;
  D_M_TLAST = 0;

end

  BER = sum/(input_data_encoder_size * no_of_samples);
  $display("\n\n\n\n\n\nBER: %0.15f\n\n\n\n\n\n", BER);

  #1000
  $stop;
end

endmodule
