quit -sim
quietly vlib work
quietly vmap work work
vcom -2008 ../../Convolutional\ Encoder/pkg/pkg_parameters_cc.vhd
vcom -2008 ../../Convolutional\ Encoder/src/delay_buffer.vhd
vcom -2008 ../../Convolutional\ Encoder/src/encoder.vhd
vcom -2008 ../../Convolutional\ Encoder/src/slave_axi_s_interface.vhd
vcom -2008 ../../Convolutional\ Encoder/src/master_axi_s_interface.vhd
vcom -2008 ../../Convolutional\ Encoder/src/axis_conv_encoder.vhd
vcom -2008 ../../Viterbi\ Decoder/pkg/pkg_func.vhd
vcom -2008 ../../Viterbi\ Decoder/pkg/pkg_parameters.vhd
vcom -2008 ../../Viterbi\ Decoder/pkg/pkg_data_types.vhd
vcom -2008 ../../Viterbi\ Decoder/src/BMU.vhd
vcom -2008 ../../Viterbi\ Decoder/src/ACS.vhd
vcom -2008 ../../Viterbi\ Decoder/src/RAM_mem.vhd
vcom -2008 ../../Viterbi\ Decoder/src/start_node.vhd
vcom -2008 ../../Viterbi\ Decoder/src/SMU.vhd
vcom -2008 ../../Viterbi\ Decoder/src/Viterbi.vhd
vcom -2008 ../../Viterbi\ Decoder/src/axis_viterbi_decoder.vhd
vlog ../topModule.sv ../../../C/wrappers.c ../../../C/encoder/encoder.c ../../../C/channel/BSC.c ../../../C/decoder/decoder.c