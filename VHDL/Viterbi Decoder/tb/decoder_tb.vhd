library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

library work;
use work.pkg_parameters.all;
use work.pkg_func.all;
use work.pkg_data_types.all;

entity axis_viterbi_decoder_tb is
end;

architecture bench of axis_viterbi_decoder_tb is

  component axis_viterbi_decoder
       Port ( clk           : in std_logic;
              ARESET_N      : in std_logic;
              M_TVALID      : in std_logic;
              M_TREADY      : out std_logic;
              M_TDATA       : in std_logic_vector(input_size - 1 downto 0);
              M_TSTRB       : in std_logic;
              M_TKEEP       : in std_logic;
              M_TLAST       : in std_logic;
              S_TVALID      : out std_logic;
              S_TREADY      : in  std_logic;
              S_TDATA       : out std_logic_vector(output_size - 1 downto 0);
              S_TSTRB       : out std_logic;
              S_TKEEP       : out std_logic;
              S_TLAST       : out std_logic
          );
  end component;

  signal clk: std_logic;
  signal ARESET_N: std_logic;
  signal M_TVALID: std_logic;
  signal M_TREADY: std_logic;
  signal M_TDATA: std_logic_vector(input_size - 1 downto 0);
  signal M_TSTRB: std_logic;
  signal M_TKEEP: std_logic;
  signal M_TLAST: std_logic;
  signal S_TVALID: std_logic;
  signal S_TREADY: std_logic;
  signal S_TDATA: std_logic_vector(output_size - 1 downto 0);
  signal S_TSTRB: std_logic;
  signal S_TKEEP: std_logic;
  signal S_TLAST: std_logic ;
  
  constant clock_period: time := 10 ns;
  signal stop_the_clock: boolean;

begin

  uut: axis_viterbi_decoder port map ( clk      => clk,
                                       ARESET_N => ARESET_N,
                                       M_TVALID => M_TVALID,
                                       M_TREADY => M_TREADY,
                                       M_TDATA  => M_TDATA,
                                       M_TSTRB  => M_TSTRB,
                                       M_TKEEP  => M_TKEEP,
                                       M_TLAST  => M_TLAST,
                                       S_TVALID => S_TVALID,
                                       S_TREADY => S_TREADY,
                                       S_TDATA  => S_TDATA,
                                       S_TSTRB  => S_TSTRB,
                                       S_TKEEP  => S_TKEEP,
                                       S_TLAST  => S_TLAST );

  stimulus: process
  begin
  
    -- Put initialisation code here
    M_TVALID <= '0';
    M_TDATA <= (others => '0');
    M_TLAST <= '0';
    S_TREADY <= '1';
    M_TSTRB <= '1';
    M_TKEEP <= '1'; 
    ARESET_N <= '0';
    wait for 5 ns;
    ARESET_N <= '1';
    wait for 20 ns;

    if(M_TREADY = '1') then
        --M_TLAST <= '1';
        M_TVALID <= '1';
        M_TDATA <= "1100001110000100111101110100010100110111101001111011100000110001011010011010010100101000110011111110100000010110011101010111000100011010001000011101111110001001100000010110011110110101010010100011001111001111000010011100011100110110010111001010011101101100";
        --M_TDATA <= "0011100000110010000100011011011101110111010000011010010001000110001111100100111001111011110010000100010110000101011010001111101001010100110111111000001111001111000010010100110111110101101111100011010001010110101110101100001110001110100110001000001100010100";
        --M_TDATA <= "1100101011100000";
        wait for 10 ns;
        --M_TLAST <= '0';
        M_TVALID <= '0';
    end if;
    
    wait until M_TREADY = '1';
--    ARESET_N <= '0';
--    wait for 5 ns;
--    ARESET_N <= '1';
--    wait for 20 ns;
    
    if(M_TREADY = '1') then
        --M_TLAST <= '1';
        M_TVALID <= '1';
        M_TDATA <= "1110111000010001011000010110000101011001101111011000111110101011100110011011110101101100000011010101100101101000111101111110111011001010001011100010100110001000001111111001110110101010100100010010000000010000011001111111111010011011001110111000101010110100";
        --M_TDATA <= "0011100000110010000100011011011101110111010000011010010001000110001111100100111001111011110010000100010110000101011010001111101001010100110111111000001111001111000010010100110111110101101111100011010001010110101110101100001110001110100110001000001100010100";
        --M_TDATA <= "1100101011100000";
        wait for 10 ns;
        --M_TLAST <= '0';
        M_TVALID <= '0';
    end if;
    
    wait;
  end process;

  clocking: process
  begin
    while not stop_the_clock loop
      clk <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;
  
end;