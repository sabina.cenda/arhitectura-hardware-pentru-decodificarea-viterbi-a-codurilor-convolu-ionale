library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

library work;
use work.pkg_parameters.all;


entity ACS is
    Port ( clk                  : in std_logic;
           rst                  : in std_logic;
           
           state                : in std_logic_vector(k_length - 2 downto 0);
           BM_low               : in std_logic_vector(BM_width downto 0);
           PM_low               : in std_logic_vector(PM_width downto 0);
           BM_high              : in std_logic_vector(BM_width downto 0);
           PM_high              : in std_logic_vector(PM_width downto 0);
           in_valid_low         : in std_logic;
           in_valid_high        : in std_logic;
           
           decision             : out std_logic;
           PM_out               : out std_logic_vector(PM_width downto 0);
           out_valid            : out std_logic
           );
end ACS;

architecture Behavioral of ACS is

    signal PM_reg, PM_nxt   : std_logic_vector(PM_width downto 0);
    signal dec_reg, dec_nxt : std_logic;
    
begin
                               
seq_process: PROCESS(clk)

	variable sum_low, sum_high    : unsigned(PM_width downto 0);
	
BEGIN

    if(clk'event and clk='1') then 
			if(rst = '0') then 
			
			  -- setting path metrics at the beginning of the trellis - all but node 0 to a large value
		      PM_reg <= (others => '0');
		      if(to_integer(unsigned(state(k_length - 2 downto 0))) /= 0) then
		        PM_reg <= std_logic_vector(to_unsigned(rate * input_size, PM_width + 1));
		      end if;
		      dec_reg <= '0';
		      
		    else
		    
		      PM_reg <= PM_nxt;
		      dec_reg <= dec_nxt;
              
		    end if;
    end if;
END PROCESS;

comb_process: PROCESS(in_valid_low, BM_low, in_valid_high, PM_low, BM_high, PM_high, PM_reg, dec_reg)

	variable sum_low, sum_high    : unsigned(PM_width downto 0);
	
BEGIN

  PM_nxt <= PM_reg;
  dec_nxt <= dec_reg;

    -- Add
  if(in_valid_low = '1' and in_valid_high = '1') then
    sum_low := unsigned(BM_low) + unsigned(PM_low);
    sum_high := unsigned(BM_high) + unsigned(PM_high);
   
    -- Compare and Select
    if(sum_low < sum_high) then
        PM_nxt <= std_logic_vector(sum_low);
        dec_nxt <= '0';
    else
        PM_nxt <= std_logic_vector(sum_high);
        dec_nxt <= '1';
    end if;
  end if;

END PROCESS;

  out_valid <= in_valid_low and in_valid_high; 
  PM_out <= PM_reg;
  decision <= dec_reg;

end Behavioral;

