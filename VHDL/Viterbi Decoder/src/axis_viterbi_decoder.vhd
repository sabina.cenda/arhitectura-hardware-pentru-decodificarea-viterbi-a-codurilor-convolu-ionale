library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

library work;
use work.pkg_parameters.all;
use work.pkg_func.all;
use work.pkg_data_types.all;


entity axis_viterbi_decoder is
     Port ( clk           : in std_logic;
            ARESET_N      : in std_logic;
        
            M_TVALID      : in std_logic;
            M_TREADY      : out std_logic;
            M_TDATA       : in std_logic_vector(input_size - 1 downto 0);
            M_TSTRB       : in std_logic;
            M_TKEEP       : in std_logic;
            M_TLAST       : in std_logic;
            
            S_TVALID      : out std_logic;
            S_TREADY      : in  std_logic;
            S_TDATA       : out std_logic_vector(output_size - 1 downto 0);
            S_TSTRB       : out std_logic;
            S_TKEEP       : out std_logic;
            S_TLAST       : out std_logic
        );
end axis_viterbi_decoder;

architecture Behavioral of axis_viterbi_decoder is

  component Viterbi
      Port ( clk       : in std_logic;
             rst       : in std_logic;
  	       in_tvalid   : in std_logic;
  	       in_tdata    : in std_logic_vector(input_size  - 1 downto 0);
  	       in_tlast    : in std_logic;
  	       in_tready   : out std_logic;
  	       out_tvalid  : out std_logic;
  	       out_tdata   : out std_logic_vector(output_size  - 1 downto 0);
  	       out_tlast   : out std_logic;
  	       out_tready  : in  std_logic
            );
  end component;

  signal TVALID: std_logic;

begin

  dec: Viterbi port map ( clk        => clk,
                          rst        => ARESET_N,
                          
                          in_tvalid  => M_TVALID,
                          in_tdata   => M_TDATA,
                          in_tlast   => M_TLAST,
                          in_tready  => M_TREADY,
                          
                          out_tvalid => TVALID,
                          out_tdata  => S_TDATA,
                          out_tlast  => S_TLAST,
                          out_tready => S_TREADY );

  S_TSTRB <= M_TSTRB;
  S_TKEEP <= M_TKEEP;
  S_TVALID <= TVALID;
  
end;
  