library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

library work;
use work.pkg_parameters.all;


entity BMU is
    Generic( branch             : in std_logic_vector(k_length - 1 downto 0) := "00000");
    Port ( clk                  : in std_logic;
           rst                  : in std_logic;
           received_sequence    : in std_logic_vector(rate - 1 downto 0);
           in_valid             : in std_logic;
           BM                   : out std_logic_vector(BM_width downto 0);
           out_valid            : out std_logic);
end BMU;

architecture Behavioral of BMU is

  signal valid, valid_nxt  : std_logic;
  signal metric, metric_nxt : std_logic_vector(BM_width downto 0);

begin
                               
seq_process: PROCESS(clk)
BEGIN

    if(clk'event and clk='1') then 
			if(rst = '0') then 
		      valid <= '0';
		      metric <= (others => '0');
            else
		      valid <= valid_nxt;
		      metric <= metric_nxt;
		    end if;
    end if;
END PROCESS;

comb_process: PROCESS(received_sequence, in_valid)

	variable parity_bits : std_logic_vector(rate - 1 downto 0);
	variable count       : unsigned(BM_width downto 0);
	
BEGIN

  if(in_valid = '1') then
		      
    count := (others => '0');

    -- compute parity bits for the received branch index
    parity_bits(0) := PARITY_POLYNOMIALS(0)(k_length - 1) and branch(0);
    parity_bits(1) := PARITY_POLYNOMIALS(1)(k_length - 1) and branch(0);
    for i in 1 to k_length - 1 loop
        parity_bits(0) := parity_bits(0) xor (branch(i) and PARITY_POLYNOMIALS(0)(i - 1));
        parity_bits(1) := parity_bits(1) xor (branch(i) and PARITY_POLYNOMIALS(1)(i - 1));  
    end loop;
                
    -- compute Hamming distance
    for i in 0 to rate - 1 loop
        if(parity_bits(i) /= received_sequence(i)) then
            count := count + 1;
        end if;
    end loop;
            
    valid_nxt <= '1';
    metric_nxt <= std_logic_vector(count);
  else 
    valid_nxt <= '0';
  end if;

END PROCESS;

  out_valid <= valid;
  BM <= metric;

end Behavioral;

