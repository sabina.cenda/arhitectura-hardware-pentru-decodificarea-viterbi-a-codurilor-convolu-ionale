library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

library work;
use work.pkg_parameters.all;
use work.pkg_func.all;
use work.pkg_data_types.all;

entity start_node is
    Port ( clk                  : in std_logic;
           rst                  : in std_logic;
           in_data              : PM_array;
           in_valid             : in std_logic;
           out_data             : out std_logic_vector(k_length - 2 downto 0);
           out_valid            : out std_logic);
end start_node;

architecture Behavioral of start_node is

  component delay_buffer
        generic(DELAY   : natural := 1;
                SIZE    : natural := 1);
        port(clk, rst   : in std_logic;
            data_in     : in std_logic_vector(SIZE - 1 downto 0);
            data_out    : out std_logic_vector(SIZE - 1 downto 0));
    end component;

  signal node, node_nxt : unsigned(k_length - 2 downto 0);
  signal arr            : PM_array;
  
  signal inv:             std_logic_vector(0 downto 0);
  signal outv:            std_logic_vector(0 downto 0);

begin

db: delay_buffer generic map (    DELAY    => 2,
                                  SIZE     =>  1)
                       port map ( clk      => clk,
                                  rst      => rst,
                                  data_in  => inv,
                                  data_out => outv );

seq_process: PROCESS(clk)
BEGIN

    if(clk'event and clk='1') then 
			if(rst = '0') then 
			
		      node <= (others => '1');

		    else
		    
		      node <= node_nxt;
		    
		    end if;
    end if;
END PROCESS;


comb_process: PROCESS(in_data, in_valid, node)

  variable min  : unsigned(PM_width downto 0);
  variable idx  : unsigned(k_length - 2 downto 0);
  
BEGIN
    node_nxt <= node;
    
    if(in_valid = '1') then 
      arr <= in_data;
    end if;
    
    min := unsigned(arr(0));
    idx :=  to_unsigned(0, k_length - 1);
    
    for i in 1 to no_of_states - 1 loop
      
        if(unsigned(arr(i)) < min) then
        
          min := unsigned(arr(i));
          idx :=  to_unsigned(i, k_length - 1);
          
        end if;
        
      end loop;
    
    node_nxt <= idx;
    
END PROCESS;

  inv(0) <= in_valid;
  out_valid <= outv(0);
  out_data <= std_logic_vector(node);

end Behavioral;