library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

library work;
use work.pkg_parameters.all;
use work.pkg_func.all;
use work.pkg_data_types.all;


entity Viterbi is
    Port ( clk         : in std_logic;
           rst         : in std_logic;
           
	       in_tvalid   : in std_logic;
	       in_tdata    : in std_logic_vector(input_size  - 1 downto 0);
	       in_tlast    : in std_logic;
	       in_tready   : out std_logic;

	       out_tvalid  : out std_logic;
	       out_tdata   : out std_logic_vector(output_size  - 1 downto 0);
	       out_tlast   : out std_logic;
	       out_tready  : in  std_logic
          );
end Viterbi;

architecture Behavioral of Viterbi is

    component BMU
      Generic(
             branch               : in std_logic_vector(k_length - 1 downto 0));
      Port ( clk                  : in std_logic;
             rst                  : in std_logic;
             received_sequence    : in std_logic_vector(rate - 1 downto 0);
             in_valid             : in std_logic;
             BM                   : out std_logic_vector(BM_width downto 0);
             out_valid            : out std_logic);
    end component;
    
    component ACS
      Port ( clk                  : in std_logic;
             rst                  : in std_logic;
           
             state                : in std_logic_vector(k_length - 2 downto 0);
             BM_low               : in std_logic_vector(BM_width downto 0);
             PM_low               : in std_logic_vector(PM_width downto 0);
             BM_high              : in std_logic_vector(BM_width downto 0);
             PM_high              : in std_logic_vector(PM_width downto 0);
             in_valid_low         : in std_logic;
             in_valid_high        : in std_logic;
           
             decision             : out std_logic;
             PM_out               : out std_logic_vector(PM_width downto 0);
             out_valid            : out std_logic
           );
    end component;
    
    component SMU
      Port ( clk                : in std_logic;
             rst                : in std_logic;
  	       in_tvalid            : in std_logic;
  	       in_tdata             : in std_logic_vector(no_of_states - 1 downto 0);
  	       in_traceback_en      : in std_logic;
  	       in_start_node        : in std_logic_vector(k_length - 2 downto 0);
  	       out_tvalid           : out std_logic;
  	       out_tdata            : out std_logic_vector(output_size  - 1 downto 0)
            );
    end component;
    
    component start_node
        port (clk       : in std_logic;
              rst       : in std_logic;
              in_data   : in PM_array;
              in_valid  : in std_logic;
              out_data  : out std_logic_vector (k_length - 2 downto 0);
              out_valid : out std_logic);
    end component;

    signal ready, ready_nxt         : std_logic;

---------------------------------------
	-- Viterbi signals
	-------------------------------------

    signal data, data_nxt           : std_logic_vector(input_size - 1 downto 0);
    signal cnt, cnt_nxt             : unsigned(log2(output_size) downto 0);
    
---------------------------------------
	-- BMU signals
	-------------------------------------

    signal received                 : std_logic_vector(rate - 1 downto 0);
    signal received_valid           : std_logic;
    signal BMs                      : BM_array;
    signal BMU_out_valid            : std_logic_vector(no_of_transitions - 1 downto 0);
    
    signal received_nxt             : std_logic_vector(rate - 1 downto 0);
    signal received_valid_nxt       : std_logic;
    signal BMs_nxt                  : BM_array;
    signal BMU_out_valid_nxt        : std_logic_vector(no_of_transitions - 1 downto 0);
    
---------------------------------------
	-- ACS signals
	-------------------------------------
    
    signal rstACS, rstACS_nxt       : std_logic;
    signal PMs                      : PM_array;
    signal decisions                : std_logic_vector(no_of_states - 1 downto 0);
    signal ACS_out_valid            : std_logic_vector(no_of_states - 1 downto 0);
    
    signal PMs_nxt                  : PM_array;
    signal PMs_in                   : PM_array;
    signal decisions_nxt            : std_logic_vector(no_of_states - 1 downto 0);
    signal ACS_out_valid_nxt        : std_logic_vector(no_of_states - 1 downto 0);
    
---------------------------------------
	-- start_node signals
	-------------------------------------
	
	signal in_valid_sn              : std_logic;
	signal node                     : std_logic_vector(k_length - 2 downto 0);
	signal node_valid               : std_logic;
    
---------------------------------------
	-- SMU signals
	-------------------------------------
    
    signal decisions_valid          : std_logic;
    signal decisions_valid_nxt      : std_logic;
    signal in_traceback_en          : std_logic;
    signal in_traceback_en_nxt      : std_logic;
    signal survivor                 : std_logic_vector(output_size - 1 downto 0);
    signal survivor_valid           : std_logic;
    
begin

---------------------------------------
	-- Branch metric unit
	-------------------------------------

	gen_branch_metrics : for i in 0 to no_of_transitions - 1 generate
	begin
		inst_branch_metrics : BMU
		      generic map ( branch         =>  std_logic_vector(to_unsigned(i, k_length)))
              port map ( clk               => clk,
                         rst               => rst,
                         received_sequence => received,
                         in_valid          => received_valid,
                         BM                => BMs_nxt(i),
                         out_valid         => BMU_out_valid_nxt(i) );
	end generate gen_branch_metrics;
	
---------------------------------------
	-- Add compare select unit
	-------------------------------------
	
	gen_path_metrics : for i in 0 to no_of_states - 1 generate
	    constant bits_i        : std_logic_vector(k_length - 2 downto 0) := std_logic_vector(to_unsigned(i, k_length - 1));
	    constant bits_int_i    : integer := to_integer(unsigned(bits_i(k_length - 3 downto 0)));
	    constant idx_low_i     : integer := bits_int_i * 2;
	    constant idx_high_i    : integer := bits_int_i * 2 + 1;
	    constant bm_idx_low_i  : integer := (idx_low_i * rate) + to_integer(unsigned(bits_i(k_length - 2 downto k_length - 2)));
	    constant bm_idx_high_i : integer := (idx_high_i * rate) + to_integer(unsigned(bits_i(k_length - 2 downto k_length - 2)));
	begin
	   
		inst_path_metrics : ACS
		      port map ( clk            => clk,
                         rst            => rstACS,
                         state          => bits_i,
                         BM_low         => BMs(bm_idx_low_i),
                         PM_low         => PMs_in(idx_low_i),
                         BM_high        => BMs(bm_idx_high_i),
                         PM_high        => PMs_in(idx_high_i),
                         in_valid_low   => BMU_out_valid(bm_idx_low_i),
                         in_valid_high  => BMU_out_valid(bm_idx_high_i),
                         decision       => decisions_nxt(i),
                         PM_out         => PMs_nxt(i),
                         out_valid      => ACS_out_valid_nxt(i));
    end generate gen_path_metrics;
    
-------------------------------------------------------------------
	-- Module to compute the starting node for the traceback process
	-----------------------------------------------------------------
	
    sn : start_node
    port map (clk       => clk,
              rst       => rst,
              in_data   => PMs,
              in_valid  => in_valid_sn,
              out_data  => node,
              out_valid => node_valid);

---------------------------------------
	-- Survivor memory unit
	-------------------------------------
	
	survivor_mem: SMU port map ( clk    => clk,
                      rst               => rst,
                      in_tvalid         => decisions_valid,
                      in_tdata          => decisions,
                      in_traceback_en   => in_traceback_en,
                      in_start_node     => node,
                      out_tvalid        => survivor_valid, 
                      out_tdata         => survivor );

seq_process: PROCESS(clk)
	
BEGIN

    if(clk'event and clk='1') then 
			if(rst = '0') then 
			
			  ready <= '1';
			  
			  data <= (others => '0');
			  
			  received <= (others => '0');
		      received_valid <= '0';
		      BMs <= (others => (others => '0'));
		      BMU_out_valid <= (others => '0');
		      
			  rstACS <= '0';
			  decisions <= (others => '0');
		      ACS_out_valid <= (others => '0');
		      
		      decisions_valid <= '0';
		      in_traceback_en <= '0';
		      
		    else
		      ready <= ready_nxt;
		      
		      data <= data_nxt;
		      cnt <= cnt_nxt;

              received <= received_nxt;
		      received_valid <= received_valid_nxt;
		      BMs <= BMs_nxt;
		      BMU_out_valid <= BMU_out_valid_nxt;
		      
		      rstACS <= rstACS_nxt;
		      decisions <= decisions_nxt;
		      ACS_out_valid <= ACS_out_valid_nxt;
		      PMs <= PMs_nxt;
		      
		      decisions_valid <= decisions_valid_nxt;
		      in_traceback_en <= in_traceback_en_nxt;
		      
		    end if;
    end if;
    
END PROCESS;

comb_process: PROCESS(in_tvalid, in_tdata, in_tlast, out_tready, ready, data, cnt, received, received_valid, BMs, BMU_out_valid, decisions, ACS_out_valid, PMs, in_traceback_en, survivor_valid)
    variable decv : std_logic := '1';
BEGIN
  in_valid_sn <= '0';
  decv := '1';

  ready_nxt <= ready;
  
  data_nxt <= data;
  cnt_nxt  <= cnt;
  
  received_valid_nxt <= '0';
  
  rstACS_nxt <= '1';
  
  in_traceback_en_nxt <= in_traceback_en; 

  -- if input data is valid prepare for decoding
  if(in_tvalid = '1') then
  
    data_nxt <= in_tdata;
    cnt_nxt <= (others => '0');
    received_valid_nxt <= '0';
    ready_nxt <= '0';
    
  else
  
    -- prepare data for BMU and ACS units
    if(cnt < output_size) then
    
        received_nxt <= data(input_size - 1 downto input_size - rate);
        received_valid_nxt <= '1';
        data_nxt(input_size - 1 downto rate) <= data(input_size - 1 - rate downto 0);
        cnt_nxt <= cnt + 1; 
        
    end if;
    
    -- if trellis is finished compute starting node for traceback
    if(cnt = output_size + 3) then
    
      in_valid_sn <= '1';
      
    end if;
    
    -- inside trellis
    if(cnt < output_size + 4) then
      cnt_nxt <= cnt + 1;
    end if;
    
    -- trellis done, start tarceback
    if(cnt = output_size + 4) then
      in_valid_sn <= '0';
      in_traceback_en_nxt <= '1'; 
      cnt_nxt <= cnt + 1;
    end if;
    
    if(cnt > output_size + 4) then
      in_traceback_en_nxt <= '0';
    end if;
  end if;
  
  -- compute if all decisions of current step are valid to be written in RAM
  for i in 0 to no_of_states - 1 loop
    decv := decv and ACS_out_valid(i);
  end loop;
  decisions_valid_nxt <= decv;
  
  if(survivor_valid = '1') then 
    ready_nxt <= '1';
    rstACS_nxt <= '0';
  end if;
  
END PROCESS;

  PMs_in <= PMs_nxt;
  out_tdata <= survivor;
  out_tvalid <= survivor_valid and not ready;
  out_tlast <= survivor_valid and not ready;
  in_tready <= ready;

end Behavioral;