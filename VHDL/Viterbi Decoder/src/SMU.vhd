library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

library work;
use work.pkg_parameters.all;
use work.pkg_func.all;
use work.pkg_data_types.all;


entity SMU is
    Port ( clk              : in std_logic;
           rst              : in std_logic;
           
	       in_tvalid        : in std_logic;
	       in_tdata         : in std_logic_vector(no_of_states - 1 downto 0);
	       
	       in_traceback_en  : in std_logic;
	       in_start_node    : in std_logic_vector(k_length - 2 downto 0);

	       out_tvalid       : out std_logic;
	       out_tdata        : out std_logic_vector(output_size  - 1 downto 0)
          );
end SMU;

architecture Behavioral of SMU is

    component RAM_mem is
      Generic( 
             number_of_locations  : in natural := 16;
             data_size            : in natural := 10); -- RAM 16x10
      Port ( clk                  : in std_logic;
             rst                  : in std_logic;
             data_in              : in std_logic_vector(data_size - 1 downto 0);
             address              : in std_logic_vector(log2(number_of_locations) - 1 downto 0);
             wr_en                : in std_logic;
             rd_en                : in std_logic;
             data_out             : out std_logic_vector(data_size - 1 downto 0));
    end component;
    
    -- store decoded message
    signal survivor_path          : std_logic_vector(output_size - 1 downto 0);
    signal survivor_path_nxt      : std_logic_vector(output_size - 1 downto 0);
    
    -- signals for reading and writing decisions to/from RAM memory
    signal cnt, cnt_nxt           : unsigned(log2(output_size) - 1 downto 0);
    signal read_mem               : std_logic;
    signal read_mem_nxt           : std_logic;
    signal decisions_out          : std_logic_vector(no_of_states - 1 downto 0);
    signal decisions_out_nxt      : std_logic_vector(no_of_states - 1 downto 0);
    
    -- signals for the traceback process
    signal node, node_nxt         : unsigned(k_length - 2 downto 0);    
    signal cnt_out, cnt_out_nxt   : unsigned(log2(output_size) downto 0);
    signal en, en_nxt             : std_logic;
    signal done, done_nxt         : std_logic;
    
begin

-------------------------------------------
	    -- Decision memory
	    -------------------------------------
	
	    dec_memory: RAM_mem 
	          generic map ( number_of_locations => output_size,
                            data_size           =>  no_of_states)
              port map ( clk            => clk,
                         rst            => rst,
                         data_in        => in_tdata,
                         address        => std_logic_vector(cnt),
                         wr_en          => in_tvalid,
                         rd_en          => read_mem,
                         data_out       => decisions_out_nxt);
                         
    
                               
seq_process: PROCESS(clk)
	
BEGIN

    if(clk'event and clk='1') then 
			if(rst = '0') then 
			
			  survivor_path <= (others => '0');
			
		      cnt <= (others => '0');
		      read_mem <= '0';
		      decisions_out <= (others => '0');
		      
		      node <= (others => '0');
		      cnt_out <= (others => '0');
		      en <= '0';
		      done <= '0';
		      
		    else
		    
		      survivor_path <= survivor_path_nxt;
		      
              cnt <= cnt_nxt;
              read_mem <= read_mem_nxt;
              decisions_out <= decisions_out_nxt;
              
              node <= node_nxt;
              cnt_out <= cnt_out_nxt;
              en <= en_nxt;
              done <= done_nxt;
              
		    end if;
    end if;
END PROCESS;

comb_process: PROCESS(in_tvalid, in_tdata, in_traceback_en, in_start_node, survivor_path, cnt, read_mem, decisions_out, node, cnt_out, en, done)

BEGIN

  survivor_path_nxt <= survivor_path;

  cnt_nxt <= cnt;
  read_mem_nxt <= read_mem;
  
  cnt_out_nxt <= cnt_out;
  en_nxt <= en;
  done_nxt <= done;
  
  out_tvalid <= '0';
  out_tdata <= (others => '0');

  -- if decoding is finished prepare for starting traceback
  if(in_traceback_en = '1') then
  
    cnt_nxt <= to_unsigned(output_size - 1, log2(output_size));
    node_nxt <= unsigned(in_start_node);
    read_mem_nxt <= '1';
    
  else
  
    -- traceback
    if(cnt_out > 0) then
    
        done_nxt <= '0';
        
        survivor_path_nxt(output_size - 2 downto 0) <= survivor_path(output_size - 1 downto 1);
        survivor_path_nxt(output_size - 1) <= node(k_length - 2);
        node_nxt(k_length - 2 downto 1) <= node(k_length - 3 downto 0);
        node_nxt(0) <= decisions_out(to_integer(node));
        cnt_out_nxt <= cnt_out - 1;
    
    else 
    
        -- traceback done, drive result to output
        if(cnt_out = 0) then
        
            out_tvalid <= '1';
            out_tdata <= survivor_path;
            done_nxt <= '1';
            
            survivor_path_nxt <= (others => '0');
            
        end if;
    
    end if;
  
    -- if traceback started prepare counters - synchronization and address
    if(read_mem = '1') then
      en_nxt <= '1';
      cnt_nxt <= cnt - 1;
    end if;
    
    if(en = '1') then
      en_nxt <= '0';
      cnt_nxt <= cnt - 1;
      cnt_out_nxt <= to_unsigned(output_size - 1, log2(output_size) + 1) + 1;
    end if;
    
    if(cnt < to_unsigned(output_size - 1, log2(output_size))) then
      en_nxt <= '0';
    end if;
  
    if(cnt = 0) then
        read_mem_nxt <= '0';
    end if;
    
    if(cnt_out = 1) then
      cnt_nxt <= (others => '0');
    end if;
  
  end if;
  
  if(en = '0' and in_tvalid = '1') then
    cnt_nxt <= cnt + 1;
  end if;
  
  if(done = '1') then
    out_tvalid <= '0';
    out_tdata <= (others => '0');
  end if;

END PROCESS;

end Behavioral;