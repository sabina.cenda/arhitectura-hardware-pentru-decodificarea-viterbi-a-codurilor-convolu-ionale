library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

library work;
use work.pkg_parameters.all;
use work.pkg_func.all;

entity RAM_mem is
    Generic( number_of_locations: in natural := 16;
             data_size          : in natural := 10); -- RAM 16x10
    Port ( clk                  : in std_logic;
           rst                  : in std_logic;
           data_in              : in std_logic_vector(data_size - 1 downto 0);
           address              : in std_logic_vector(log2(number_of_locations) - 1 downto 0);
           wr_en                : in std_logic;
           rd_en                : in std_logic;
           data_out             : out std_logic_vector(data_size - 1 downto 0));
end RAM_mem;

architecture Behavioral of RAM_mem is

  type ram_mem is array (0 to number_of_locations - 1) of std_logic_vector (data_size - 1 downto 0);
  signal ram_data: ram_mem;
  
begin

seq_process: PROCESS(clk)
	
BEGIN

    if(clk'event and clk='1') then 
			if(rst = '0') then 
			
		      for i in 0 to number_of_locations - 1 loop
		        ram_data(i) <= (others => '0');
		      end loop;
		      
		    else
		      
		      if(wr_en = '1') then
		        ram_data(to_integer(unsigned(address))) <= data_in;
		      end if;
		      
		      if(rd_en = '1') then
		        data_out <= ram_data(to_integer(unsigned(address)));
		      else data_out <= (others => '0');
		      end if;
		    end if;
    end if;
END PROCESS;  

end Behavioral;