library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.pkg_func.all;


package pkg_parameters is

	--
	-- Set code rate - number of parity bits
	--
	constant rate : natural := 2;
	
	--
	-- Set constraint length 
	--
	constant k_length : natural := 5;
	
	--
	-- Set parity polynomials in decimal notation
    --
    type t_parity is array (rate - 1 downto 0) of std_logic_vector(k_length - 1 downto 0);
	constant PARITY_POLYNOMIALS   : t_parity := ("10011", "11101"); -- (19, 29)dec; (23, 35)oct
	
	--
	-- Set input stream size and decoded output size
    --
    constant input_size : natural := 256;
    constant output_size : natural := input_size/rate;
	
	--
	-- Set branch metric and path metric size
    --
	constant BM_width : natural := log2(rate);
	constant PM_width : natural := log2(rate * input_size);
	
	--
	-- Set trellis parameters
    --
    constant ones               : std_logic_vector(k_length - 2 downto 0) := (others => '1');
    constant no_of_states       : natural := to_integer(unsigned(ones)) + 1;
    constant no_of_transitions  : natural := no_of_states * 2;

end package pkg_parameters;