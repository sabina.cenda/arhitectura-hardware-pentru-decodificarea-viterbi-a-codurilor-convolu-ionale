library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.pkg_parameters.all;


package pkg_data_types is

	--
	-- BM and PM arrays
	--
	
	type BM_array is array (no_of_transitions - 1 downto 0) of std_logic_vector(BM_width downto 0);
	type PM_array is array (no_of_states - 1 downto 0) of std_logic_vector(PM_width downto 0);
	
end package pkg_data_types;