library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


package pkg_func is

    function log2(value : positive) return natural;
    
end package pkg_func;

package body pkg_func is

    function log2(value : positive) return natural is
        variable result : natural := 0;
        variable temp : positive := value;
    begin
        while temp > 1 loop
            temp := temp / 2;
            result := result + 1;
        end loop;
        
        return result;
    end function;
    
end package body pkg_func;