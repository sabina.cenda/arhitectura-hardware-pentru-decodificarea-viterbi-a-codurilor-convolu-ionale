#include "decoder.h"

FILE *inputFile, *outputFile;

void readInput(char *encoded, int encoded_size){
    char c[2];
    int i;

    for(i = 0; i < encoded_size; i++){
        fread(c,sizeof(char),1, inputFile);
        if(c[0] == '0') encoded[i] = 0x00;
        else if(c[0] == '1') encoded[i] = 0xff;
    }

}

void printDecoded(char *decoded, int decoded_size){
    int i;
    for(i = 0; i < decoded_size; i++){ 
        fprintf(outputFile, "%d", -(decoded[i] % 2));
    }
    fprintf(outputFile, "\n");
}

int BMU(char *received, char *ideal, int rate){

    int HammingDistance = 0;
    int i;

    for(i = 0; i < rate; i++)
        if(received[i] != ideal[i])
            HammingDistance++;

    return HammingDistance;
}

nodeData ACS(int BM_low, int PM_low, int BM_high, int PM_high){

    //_low is branch with '0' (last bit, the one lost in the transition), _high is branch with '1' 
    
    nodeData node;
    int sum_low = BM_low + PM_low;
    int sum_high = BM_high + PM_high;

    if(sum_low < sum_high){
        node.pathMetric = sum_low;
        node.decision = 0x00;
    }
    else{
        node.pathMetric = sum_high;
        node.decision = 0xff; 
    }

    return node;
}

char* encode_s(int state, char input_bit, int rate, int no_of_states, int eq[], int k){
    char *ideal;
    ideal = (char*)malloc(rate * sizeof(char));
    if(ideal == NULL){
        printf("Error on ideal sequence memory allocation\n");
        exit(EXIT_FAILURE);
    }
    
    int in_bit;
    if(input_bit == 0x00) in_bit = 0;
    else in_bit = 1;

    int bits = (in_bit << (k - 1)) + state;
    int parity_bit, parity;
    int i;

    for(i = 0; i < rate; i++){
        parity = 0;
        parity_bit = bits & eq[i];
        while (parity_bit != 0) {
            parity ^= (parity_bit & 1);
            parity_bit >>= 1;
        }
        if(parity == 0) ideal[i] = 0x00;
        else ideal[i] = 0xff;
    }

    return ideal;
}

char** memoryUpdate(char **decisions, char *currentStepDecisions, int step, int no_of_states ){

    if(step == 0)
        decisions = (char**)malloc((step + 1) * sizeof(char*));
    else
        decisions = (char**)realloc(decisions, (step + 1) * sizeof(char*));

    if(decisions == NULL){
        printf("Error on decisions memory allocation at step %d\n", step);
        exit(EXIT_FAILURE);
    }

    decisions[step] = (char*)malloc(no_of_states * sizeof(char));
    if(decisions[step] == NULL){
        printf("Error on decisions memory allocation\n");
        exit(EXIT_FAILURE);
    }

    int i;
    for(i = 0; i < no_of_states; i++){
        decisions[step][i] = currentStepDecisions[i];
    }

    return decisions;
}

void SMU(char **decisions, int *PMs, char *decoded, int decoded_size, int no_of_states, int k){
    int min = INT_MAX/4;
    int position = INT_MAX/4;

    int i;
    for(i = 0; i < no_of_states; i++){
        if(PMs[i] < min){
            min = PMs[i];
            position = i;
        }
    }

    int val;
    char bit;

    for(i = decoded_size - 1; i >= 0; i--){
        //get decoded bit
        val = position >> (k - 2);
        if(val == 0) bit = 0x00;
        else bit = 0xff;
        decoded[i] = bit;

        //compute next node
        bit = decisions[i][position];
        if(bit == 0x00) val = 0;
        else val = 1;
        position = ((position & (~(1 << (k - 2)))) << 1) + val;
    }

}

void Viterbi(char *encoded, char *decoded, int encoded_size, int decoded_size, int rate, int k, int eq[]){

    int no_of_transitions = 1 << k;
    int no_of_states = 1 << (k - 1);
    char* transitions[no_of_transitions];

    int i;
    for(i = 0; i < no_of_transitions; i++){
        transitions[i] = (char*)malloc(rate * sizeof(char));
        if(transitions == NULL){
            printf("Error on transitions memory allocation\n");
            exit(EXIT_FAILURE);
        }
    }

    //store transitions for a trellis stage
    for(i = 0; i < no_of_states; i++){
        transitions[2 * i] = encode_s(i, 0, rate, no_of_states, eq, k);
        transitions[2 * i + 1] = encode_s(i, 1, rate, no_of_states, eq, k);
    }

    //needed for computing branch metrics
    char *received;
    received = (char*)malloc(rate * sizeof(char));
    if(received == NULL){
        printf("Error on received sequence memory allocation\n");
        exit(EXIT_FAILURE);
    }

    char *ideal;
    ideal = (char*)malloc(rate * sizeof(char));
    if(ideal == NULL){
        printf("Error on ideal sequence memory allocation\n");
        exit(EXIT_FAILURE);
    }

    //initialize path metrics
    int PM[no_of_states], PM_next[no_of_states];
    PM[0] = 0;
    for(i = 1; i < no_of_states; i++){
        PM[i] = INT_MAX/4;
    }

    int bit;
    int prev_state0, prev_state1;
    int BM0, BM1;
    nodeData node;

    //decision memory
    char **decisions;
    char currentStepDecisions[no_of_states];
    int id, j;
    for(id = 0; id < decoded_size; id++){
        //get received parity bits for current trellis stage
        for(j = 0; j < rate; j++){
            received[j] = encoded[rate * id + j];
        }

        for(i = 0; i < no_of_states; i++){
            //compute metrics for each state of the current stage
            if((i & (1 << (k - 2))) != 0) 
                bit = 1;
            else bit = 0;

            prev_state0 = (i & (~(1 << (k - 2)))) << 1;
            prev_state1 = ((i & (~(1 << (k - 2)))) << 1) + 1;

            ideal = transitions[prev_state0 * rate + bit];
            BM0 = BMU(received, ideal, rate);
            ideal = transitions[prev_state1 * rate + bit];
            BM1 = BMU(received, ideal, rate);
        
            node = ACS(BM0, PM[prev_state0], BM1, PM[prev_state1]);
            PM_next[i] = node.pathMetric;
            currentStepDecisions[i] = node.decision;
        }

        for(i = 0; i < no_of_states; i++)
            PM[i] = PM_next[i];
        decisions = memoryUpdate(decisions, currentStepDecisions, id, no_of_states);
    }
    SMU(decisions, PM, decoded, decoded_size, no_of_states, k);

}
/*
int main(void){
    char *encoded, *decoded;
    int constraint_length = 5;
    int rate = 2;
    int encoded_size = 256;
    int decoded_size = encoded_size/rate;
    int eq[] = {19, 29};

    encoded = (char*)malloc(encoded_size * sizeof(char));
    if(encoded == NULL){
        printf("Error on encoded memory allocation\n");
        exit(EXIT_FAILURE);
    }

    decoded = (char*)malloc(decoded_size * sizeof(char));
    if(decoded == NULL){
        printf("Error on decoded memory allocation\n");
        exit(EXIT_FAILURE);
    }

    if((inputFile=fopen("in.txt","r"))==NULL){
        printf("Error opening file\n");
        exit(EXIT_FAILURE);
    }
    
    if((outputFile=fopen("out.txt","w"))==NULL){
        printf("Error opening file\n");
        exit(EXIT_FAILURE);
    }

    char c[2];
    int number_of_sample_vectors = 50;
   for(int i = 0; i < number_of_sample_vectors; i++){
        readInput(encoded, encoded_size);
        //BSC(encoded, encoded_size, 0.0123);
        Viterbi(encoded, decoded, encoded_size, decoded_size, rate, constraint_length, eq);
        printDecoded(decoded, decoded_size); 
        fread(c,sizeof(char),1, inputFile);
    }

    fclose(inputFile);
    fclose(outputFile);

    return 0;
}*/
