#ifndef decoder
#define decoder

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>

typedef struct nodeData{
    int pathMetric;
    char decision;
} nodeData;

void readInput(char *encoded, int encoded_size);
void printDecoded(char *decoded, int decoded_size);
int BMU(char *received, char *ideal, int rate);
nodeData ACS(int BM_low, int PM_low, int BM_high, int PM_high);
char* encode_s(int state, char input_bit, int rate, int no_of_states, int eq[], int k);
char** memoryUpdate(char **decisions, char *currentStepDecisions, int step, int no_of_states );
void SMU(char **decisions, int *PMs, char *decoded, int decoded_size, int no_of_states, int k);
void Viterbi(char *encoded, char *decoded, int encoded_size, int decoded_size, int rate, int k, int eq[]);

#endif
