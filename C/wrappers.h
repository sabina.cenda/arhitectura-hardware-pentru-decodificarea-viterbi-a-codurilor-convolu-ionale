#include <time.h>
#include "encoder/encoder.h"
#include "decoder/decoder.h"
#include "channel/bsc.h"

#include "svdpi.h"

FILE *inputFile;
void generateVector(char *bits, int bits_size);
extern void readInputWrapper(svLogicVecVal vsource[128], int source_size);
extern void convEncoderWrapper(svLogicVecVal vsource[128], svLogicVecVal vcoded[256], int source_size, int coded_size, int k_length, int rate, int eq[2]);
extern void bscWrapper(svLogicVecVal bitsIn[256], svLogicVecVal bitsOut[256], int bits_size, double probability);
extern void viterbiDecoderWrapper(svLogicVecVal encoded[256], svLogicVecVal decoded[128], int encoded_size, int decoded_size, int rate, int k_length, int eq[2]);
