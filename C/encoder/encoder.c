#include "encoder.h"

FILE *inputFile, *outputFile;

void readInpute(char *source, int source_size){
    char c[2];
    int i;

    for(i = 0; i < source_size; i++){
        fread(c,sizeof(char),1, inputFile);
        if(c[0] == '0') source[i] = 0x00;
        else if(c[0] == '1') source[i] = 0xff;
    }
}

void conv_encoder(char *source, char *coded, int source_size, int coded_size, int constraint_length, int rate, int *eq){
    char x[constraint_length], g[rate];
    int i, j, k;
    for(i = 0; i < constraint_length; i++){
        x[i] = 0x00;
    }

    for(i = 0; i < source_size; i++){
        x[constraint_length - 1] = source[i];

        for(j = 0; j < rate; j++){
            g[j] = (x[0]*(eq[j] & 1));

            for(k = 1; k < constraint_length; k++){
                g[j] = g[j]^(x[k]*((eq[j] & (1<<k))>>k));
            }
        
            coded[rate * i + j] = g[j];
        }

        for(j = 0; j < constraint_length - 1; j++){
            x[j] = x[j + 1];
        }

    }
}

void printEncoded(char *source, char *coded, int source_size, int rate){
    int i, j;
    for(i = 0; i < source_size; i++){ 
        printf("%d", -(source[i]%2));

        for(j = 0; j < rate; j++)
            fprintf(outputFile, "%d", -(coded[rate * i + j] % 2));
    }

    fprintf(outputFile, "\n");
}
/*
int main()
{
    char *source, *coded;
    int constraint_length = 5;
    int rate = 2;
    int source_size = 128;
    int coded_size =  rate * source_size;
    int eq[] = {19, 29};

    source = (char*)malloc(source_size * sizeof(char));
    if(source == NULL){
        printf("Error on source memory allocation\n");
        exit(EXIT_FAILURE);
    }

    coded = (char*)malloc(coded_size * sizeof(char));
    if(coded == NULL){
        printf("Error on encoded memory allocation\n");
        exit(EXIT_FAILURE);
    }

    if((inputFile=fopen("in.txt","r"))==NULL){
        printf("Error opening file\n");
        exit(EXIT_FAILURE);
    }
    
    if((outputFile=fopen("out.txt","w"))==NULL){
        printf("Error opening file\n");
        exit(EXIT_FAILURE);
    }

    char c[2];
    int number_of_sample_vectors = 50;
    for(int i = 0; i < number_of_sample_vectors; i++){
        readInpute(source, source_size);
        conv_encoder(source, coded, source_size, coded_size, constraint_length, rate, eq);
        printEncoded(source, coded, source_size, rate); 
        fread(c,sizeof(char),2, inputFile);
        printf("\n\n");
    }

    fclose(inputFile);
    fclose(outputFile);

    return 0;
}*/
