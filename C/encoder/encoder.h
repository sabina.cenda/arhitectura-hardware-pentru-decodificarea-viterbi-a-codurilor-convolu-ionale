#ifndef encoder
#define encoder

#include <stdio.h>
#include <stdlib.h>

void readInpute(char *source, int source_size);
void conv_encoder(char *source, char *coded, int source_size, int coded_size, int constraint_length, int rate, int *eq);
void printEncoded(char *source, char *coded, int source_size, int rate);

#endif
