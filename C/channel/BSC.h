#ifndef BSC
#define BSC

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>

void readInputm(char *bits, int bits_size);
char flipBit(char b);
void bsc(char *bits, int bits_size, float probability);
void writeOutput(char *bits, int bits_size);

#endif
