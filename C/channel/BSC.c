#include "bsc.h"

FILE *inputFile, *outputFile;

void readInputm(char *bits, int bits_size){
    char c[2];
    int i;

    for(i = 0; i < bits_size; i++){
        fread(c,sizeof(char),1, inputFile);
        if(c[0] == '0') bits[i] = 0x00;
        else if(c[0] == '1') bits[i] = 0xff;
    }

}

char flipBit(char b){
    if(b == 0x00) b = 0xff;
        else if(b == 0xff) b = 0x00;

    return b;
}

void bsc(char *bits, int bits_size, float probability){
    if(probability > 1){
        printf("Error: probability of binary symmetric channel can't be greater than 1\n");
        exit(1);
    }

    float randomProbability;
    int i;

    for(i = 0; i < bits_size; i++){
        randomProbability = (double)rand() / RAND_MAX;
        if(randomProbability < probability)
            bits[i] = flipBit(bits[i]);
    }
}

void writeOutput(char *bits, int bits_size){
    int i;
    for(i = 0; i < bits_size; i++){ 
        fprintf(outputFile, "%d", -(bits[i] % 2));
    }
    fprintf(outputFile, "\n");
}
/*
int main(void){

    char *bits;
    int bits_size = 256;

    bits = (char*)malloc(bits_size * sizeof(char));
    if(bits == NULL){
        printf("Error on encoded memory allocation\n");
        exit(EXIT_FAILURE);
    }

    if((inputFile=fopen("in.txt","r"))==NULL){
        printf("Error opening file\n");
        exit(EXIT_FAILURE);
    }
    
    if((outputFile=fopen("out.txt","w"))==NULL){
        printf("Error opening file\n");
        exit(EXIT_FAILURE);
    }

    char c[2];
    int number_of_sample_vectors = 50;
    for(int i = 0; i < number_of_sample_vectors; i++){
        readInputm(bits, bits_size);
        bsc(bits, bits_size, 0.0123);
        writeOutput(bits, bits_size); 
        fread(c,sizeof(char),1, inputFile);
    }

    fclose(inputFile);
    fclose(outputFile);

    return 0;

}*/
