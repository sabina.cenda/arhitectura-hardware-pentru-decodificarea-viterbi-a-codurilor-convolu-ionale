#include "wrappers.h"

//FILE *inputFile, *outputFile;

void generateVector(char *bits, int bits_size){

    int i;
    for (i = 0; i < bits_size; i++) {
        int random_num = rand() % 2;
        bits[i] = random_num ? 0xff : 0x00;
    }

}

extern void readInputWrapper(svLogicVecVal vsource[128], int source_size){

    char source[source_size];

    generateVector(source, source_size);

    int i;
    for(i = 0; i < source_size; i++){
        if(source[i] == -1) vsource[i].aval = 1;
        else vsource[i].bval = 0;
    }

}

extern void convEncoderWrapper(svLogicVecVal vsource[128], svLogicVecVal vcoded[256], int source_size, int coded_size, int k_length, int rate, int eq[2]){

    char source[source_size], coded[coded_size];

    int i;
    for(i = 0; i < source_size; i++){
        if(vsource[i].aval == 1) source[source_size - 1 - i] = 0xff;
        else if(vsource[i].bval == 0) source[source_size - 1 - i] = 0x00;
    }

    conv_encoder(source, coded, source_size, coded_size, k_length, rate, eq);
        
    for(i = 0; i < coded_size; i++){
        if(coded[i] == -1) vcoded[coded_size - 1 - i].aval = 1;
        else vcoded[coded_size - 1 - i].bval = 0;
    }

}

extern void bscWrapper(svLogicVecVal bitsIn[256], svLogicVecVal bitsOut[256], int bits_size, double probability){

    char bits[bits_size];

    int i;
    for(i = 0; i < bits_size; i++){
        if(bitsIn[i].aval == 1) bits[i] = 0xff;
        else if(bitsIn[i].bval == 0) bits[i] = 0x00;
    }

    bsc(bits, bits_size, probability);

    for(i = 0; i < bits_size; i++){
        if(bits[i] == -1) bitsOut[i].aval = 1;
        else bitsOut[i].bval = 0;
    }

}

extern void viterbiDecoderWrapper(svLogicVecVal encoded[256], svLogicVecVal decoded[128], int encoded_size, int decoded_size, int rate, int k_length, int eq[2]){

    char enc[encoded_size], dec[decoded_size];

    int i;
    for(i = 0; i < encoded_size; i++){
        if(encoded[i].aval == 1) enc[encoded_size - 1 - i] = 0xff;
        else if(encoded[i].bval == 0) enc[encoded_size - 1 - i] = 0x00;
    }

    Viterbi(enc, dec, encoded_size, decoded_size, rate, k_length, eq);

    for(i = 0; i < decoded_size; i++){
        if(dec[i] == -1) decoded[i].aval = 1;
        else decoded[i].bval = 0;
    }

}

int main(void){
/*
    char *source, *encoded, *decoded;
    int constraint_length = 5;
    int rate = 2;
    int source_size = 128;
    int encoded_size = 256;
    int decoded_size = source_size;
    int eq[] = {19, 29};

    source = (char*)malloc(source_size * sizeof(char));
    if(source == NULL){
        printf("Error on encoded memory allocation\n");
        exit(EXIT_FAILURE);
    }

    encoded = (char*)malloc(encoded_size * sizeof(char));
    if(encoded == NULL){
        printf("Error on encoded memory allocation\n");
        exit(EXIT_FAILURE);
    }

    decoded = (char*)malloc(decoded_size * sizeof(char));
    if(decoded == NULL){
        printf("Error on decoded memory allocation\n");
        exit(EXIT_FAILURE);
    }

    if((inputFile=fopen("in.txt","r"))==NULL){
        printf("Error opening file\n");
        exit(EXIT_FAILURE);
    }
    
    if((outputFile=fopen("out.txt","w"))==NULL){
        printf("Error opening file\n");
        exit(EXIT_FAILURE);
    }

    char c[2];
    int number_of_sample_vectors = 50, i;
   for(i = 0; i < number_of_sample_vectors; i++){
        readInputm(source, source_size);
        conv_encoder(source, encoded, source_size, encoded_size, constraint_length, rate, eq);
        BSC(encoded, encoded_size, 0.0123);
        Viterbi(encoded, decoded, encoded_size, decoded_size, rate, constraint_length, eq);
        printDecoded(decoded, decoded_size); 
        fread(c,sizeof(char),2, inputFile);
    }

    fclose(inputFile);
    fclose(outputFile);
*/
    return 0;
}
